# Some sessions' data are not like others
Maybe overall performance was bad on those sessions, or did they come from 
a specific mouse, or the brain was damaged? Let's see if we can find out.



```{toctree}
:hidden:
:titlesonly:


sessions_good
sessions_bad
```
