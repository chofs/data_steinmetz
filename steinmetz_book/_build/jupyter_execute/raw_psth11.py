# Unsorted PSTHs session 11

# HIDE CELL
from importlib import reload

import matplotlib.pylab as plt
import numpy as np

import deps
reload(deps)

alldat, dat_LFP, dat_ST = deps.get_data()
dat = alldat[11]


sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)
events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                             event_names=('gocue', 'response_time','feedback_time'),
                                             condition_names=('response','contrast_right','contrast_left','feedback_type'))

spykes_times = deps.spykes_get_times(sp, neu)

## Loop over events and conditions

# HIDE CODE
for event in events_df.columns[events_df.columns.str.contains('time')]:
    for condition in events_df.columns[~events_df.columns.str.contains('time')]:
        _, all_psth = deps.get_psth(spikes=spykes_times,
                               spykes_df=events_df,
                               event=event,
                               conditions=condition,
                               window=[-500, 1500],
                               bin_size=10,
                               )
        # Convert
        xar=deps.spyke2xar(all_psth)
        # Plot 1
        np.log(xar).plot(col=xar.dims[0],robust=False,figsize=[10,10],add_colorbar=False);
        plt.suptitle(xar.name);plt.show()
        # Plot 2
        xar.mean('Neuron').plot(hue=xar.dims[0]);
        plt.suptitle(xar.name);plt.show()
