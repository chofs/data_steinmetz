# Tanvi's GLM
## Prep

# HIDE CELL
from importlib import reload

import matplotlib.pylab as plt
import numpy as np

import deps
reload(deps)

alldat, dat_LFP, dat_ST = deps.get_data()
dat = alldat[0]
sp, neu, trial_starts = deps.convert_raster_to_spiketimes(dat)
events_df = deps.convert_events_to_dataframe(dat, trial_starts,
                                             event_names=('gocue', 'response_time','feedback_time'),
                                             condition_names=('response','contrast_right','contrast_left','feedback_type'))

spykes_times = deps.spykes_get_times(sp, neu)

dt = dat['bin_size'] # binning at 10 ms
NT = dat['spks'].shape[-1]

ax = plt.subplot(1,5,1)
response = dat['response'] # right - nogo - left (-1, 0, 1)
vis_right = dat['contrast_right'] # 0 - low - high
vis_left = dat['contrast_left'] # 0 - low - high

## Encode stimulus onset into spike trains using a Poisson GLM

#Make contrast matrix
cL = dat['contrast_left']; cR = dat['contrast_right']
cdiff = np.empty_like(cL)

for i in range(len(cL)):
    cdiff[i] = 1*(cL[i]>cR[i]) + (-1)*(cL[i] < cR[i]) + 0*(cL[i] == cR[i])

f=plt.figure(figsize=(10,5))
plt.plot(cdiff); plt.xlabel('Trials'); plt.ylabel('Contrast difference: (L) - (R)'); plt.show()
print(np.sum(cdiff<0))

# Dependencies
import pdb
from scipy.optimize import minimize
def make_design_matrix(stim,d):
    try:
        padded_stim = np.concatenate([np.zeros(d - 1), stim])
    except:
        print('Fuck')
        pdb.set_trace()
    T = len(stim)  # Total number of timepoints (hint: total number of stimulus frames)
    X = np.zeros((T, d))
    for t in range(T):
        X[t] = padded_stim[t:t+d]

    return X
    
def neg_log_like(theta,X,y,L1_penalty):
#      Args:
#     theta (1D array): Parameter vector.
#     X (2D array): Full design matrix.
#     y (1D array): Data values. Number of spikes for each trial
    
    rate = np.exp(X @ theta)
    penalty = 20*np.sum((theta)**2) #20*np.sum(np.abs(theta))
    LL = y @ np.log(rate) - np.ones_like(y) @ rate
    log_lik = LL - penalty
#     print(f' LL: {LL:1.3f}, L1: {penalty:1.3f}, log_lik: {log_lik:1.3f}')
    return -log_lik

def fit_lnp(stim,dummy,spikes,d,L1_penalty):
    y = spikes
    constant = np.ones_like(y)
    
#     pdb.set_trace()
    X = np.column_stack([constant, dummy, make_design_matrix(stim,d)])

    # Use a random vector of weights to start (mean 0, sd .2)
    theta0 = np.random.normal(0, .2, np.size(X[0]))  #d + 1)

    # Find parameters that minmize the negative log likelihood function
    res = minimize(neg_log_like, theta0, args=(X, y,L1_penalty))

    return res["x"]

def predict_spike_counts_lnp(stim,dummy,spikes,d,L1_penalty,theta=None):
    y = spikes
    constant = np.ones_like(spikes)

    if theta is None:
        theta = fit_lnp(stim,dummy,y,d,L1_penalty)
    
    #Make X again to compute predicted spikes
    X = np.column_stack([constant, dummy, make_design_matrix(stim,d)])
    yhat = np.exp(X @ theta)
    return yhat, theta



all_spikes = np.transpose(np.sum(dat['spks'][:,:,50:131],axis=2)); print(all_spikes.shape)

#Visualize vis-ctx neurons' firing rates
idx_vis = np.char.startswith(dat['brain_area'],'VIS',start=0,end=None)
vis_neurons = np.where(idx_vis)[0]

#Find neurons with high-ish spike rates and try to encode
vis_spikes = all_spikes[:,vis_neurons] #print(vis_spikes.shape)
f = plt.figure(figsize=(7,5))
mean_vis_spikes = np.mean(vis_spikes,axis=0)
plt.plot(mean_vis_spikes,'ob'); plt.title(f'Mean #spikes for the {len(vis_neurons):d} VIS neurons')
plt.xlabel('Neuron #')

#Pick neurons with a threshold firing rate/spike number
spike_thresh = 5
idx_high = np.where(mean_vis_spikes>spike_thresh)[0]
vis_neurons_thresh = vis_neurons[idx_high]

print(f'OG indices of neurons with min {spike_thresh:d} spikes'); print(vis_neurons_thresh) # to keep track of which original neuron
plt.plot(idx_high, mean_vis_spikes[idx_high],'+r',markersize=15,alpha=0.5)

# plt.savefig('Figures/Choice of neuron.png')

# Print spike trains for neuron 100
chosen_neuron = 129
spy = dat['spks'][chosen_neuron,:,50:131]
# f = plt.figure(figsize=(20,5)); plt.imshow(spy, aspect='auto'); plt.tight_layout(); plt.show()
# f= plt.figure(figsize=(20,5)); plt.hist(spy.ravel(),bins=50); plt.show()

# Supersample 2x (into 5 ms bins)
sup_spy = np.empty(shape=(np.size(spy),np.size(spy[0]*2))); print(sup_spy)

# Work with neuron 
chosen_neuron = 100 # 100 in the list of the whole 698 neurons
spikes_new = all_spikes[:,chosen_neuron] #print(np.mean(spikes_new))

# Subplot 1: How does #spikes change with contrast difference
color_list = plt.cm.jet(np.linspace(0.3,0.8,6))
f=plt.figure(figsize=(20,5))
plt.subplot(1,4,1)
for i in range(len(cdiff)):
    plt.plot(i,spikes_new[i],'o',color=color_list[int((cdiff[i]+1))],alpha=0.7,markersize=10)
    
plt.plot(np.inf,np.inf,'o',color=color_list[0],label='L<R',markersize=10)
plt.plot(np.inf,np.inf,'o',color=color_list[1],label='L=R',markersize=10)
plt.plot(np.inf,np.inf,'o',color=color_list[2],label='L>R',markersize=10)
plt.legend(loc='upper left'); plt.title('Contrast difference')
plt.xlabel('Trial #'); plt.ylabel('Number of spikes')

# Subplot 2: How does #spikes change with left contrast
color_list = plt.cm.jet(np.linspace(0.3,0,4))
plt.subplot(1,4,2)
contrast_list = np.unique(cL)
for itrial in range(len(cL)):
    idx = np.where(contrast_list == cL[itrial])[0][0]
    plt.plot(itrial, spikes_new[itrial],'o',color=color_list[idx],alpha=0.7,markersize=10)

for i in range(len(contrast_list)):
    plt.plot(np.inf,np.inf,'o',color=color_list[i],alpha=0.5,markersize=10,label=f'Contrast: {contrast_list[i]}, #: {np.sum(cL == contrast_list[i]):d}')

plt.legend(fontsize=10); plt.xlabel('Trial #'); plt.ylabel('Number of spikes')
plt.title('Left contrast')
  
    
# #Subplot 3: How does #spikes change with right contrast
color_list = plt.cm.jet(np.linspace(0.7,1,4))
plt.subplot(1,4,3)
contrast_list = np.unique(cR)
for itrial in range(len(cR)):
    idx = np.where(contrast_list == cR[itrial])[0][0]
    plt.plot(itrial, spikes_new[itrial],'o',color=color_list[idx],alpha=0.5,markersize=10)

for i in range(len(contrast_list)):
    plt.plot(np.inf,np.inf,'o',color=color_list[i],alpha=0.5,markersize=10,label=f'Contrast: {contrast_list[i]}, #: {np.sum(cR == contrast_list[i]):d}')

plt.legend(fontsize=10); plt.xlabel('Trial #'); plt.ylabel('Number of spikes')
plt.title('Right contrast')
  
# plt.savefig('Figures/Choice_of_stimulus.png')

''' 
Slice time based on stimulus onset, go cue and reward presentation time
'''
def time_slice(half_time_wind, dat):
    
    tslices = np.empty(shape=(2,),dtype='object')
    
    all_gocue_slice  = []
    all_rwd_slice = []
    
    gocue_time = (dat['gocue']*100+50).astype(int).flatten()
    rwd_time = (dat['feedback_time']*100+50).astype(int).flatten()
    
    for i in range(dat['spks'].shape[1]):
        gocue_slice = dat['spks'][:,i, gocue_time[i]-half_time_wind:gocue_time[i]+half_time_wind]
        all_gocue_slice.append(gocue_slice)
        
#         rwd_slice = dat['spks'][:,i,rwd_time[i]-half_time_wind: rwd_time[i]+half_time_wind]
#         pdb.set_trace()
#         all_rwd_slice.append(rwd_slice)

    all_gocue_slice = np.stack(all_gocue_slice, axis=1) # for session 11, output shape (698, 340, 40)
#     all_rwd_slice = np.stack(all_rwd_slice, axis=1)
    stimulus_onset_slice = dat['spks'][:,:,50-half_time_wind:50+half_time_wind] # for session 11, output shape (698, 340, 40)
    
    
    tslices[0] = stimulus_onset_slice; tslices[1] = all_gocue_slice #tslice[2] = all_rwd_slice
    
    return tslices

'''
Plot coupling weights for each brain region - coarse and fine (aggregated across sessions)
(a) Run GLM fitting across sessions
(b) Concatenate all coupling weights into one matrix
(c) Bar and color by brain region

'''
from tqdm import tqdm

# Assign constants
d = 4
dummy_type = ['Right contrast','Go/NoGo']  #Add 'Left/Right' feedback later
half_time_wind = 20; to_slice = 1
which_idx = [2,1] #for right contrast, in tt, idx=2 is coupling, and for go-nogo, idx=1 is coupling


# Initialise storage variables
rr = np.empty(shape=(1000000,len(dummy_type))) # r2 of GLM fits
tt = np.empty(shape=(1000000,d+2,len(dummy_type))) # coefficients of GLM fits for all neurons
couple_session = np.empty(shape=(39,len(dummy_type)),dtype='object') # coupling to dummy variables stored by session

# Loop variables
kr = 0; kt = 0

for isession in tqdm(range(39)):
    dat = alldat[isession]
    ntrial = len(dat['response'])
    nneuron = len(dat['spks'])
    st1 = dat['contrast_right']
    dum = np.array([dat['response']*0, abs(dat['response'])>0]).T  #, dat['feedback_type']]).T #print(sum(dum))
    
    tslices = time_slice(half_time_wind,dat)
    
    for nn in range(nneuron):
        for idum in range(0,len(dum[0])):
            if to_slice:
                y1 = np.sum(tslices[idum],axis=2).T[:,nn]
            else:
                y1 = np.sum(dat['spks'],axis=2).T[:,nn] #Use this if not slicing based on trial epoch [stim onset, go cue, rwd]
            #pdb.set_trace()
            
            y2, th = predict_spike_counts_lnp(st1,dum[:,idum],y1,d,1)
            with np.errstate(divide='ignore'):
                rr_all = np.divide((y1-y2)**2,y1**2); rr_all[np.abs(rr_all)==np.inf]=0
            
            rr[kr,idum] = np.sqrt(np.mean(rr_all))
            tt[kt,:,idum] = th

        kr+=1;kt+=1
    

rr = rr[0:kr,:]; tt = tt[0:kr,:,:]
# f=plt.figure(figsize=(15,10))
# plt.plot(rr,'o'); plt.plot()
# plt.ylabel('Prediction error'); plt.xlabel('Neuron')



brain_area = []
start = 0
for isession in range(39):
#     print(isession)
    dat = alldat[isession]
    n_neuron = len(dat['spks'])
    brain_area.append(dat['brain_area'].tolist())
    
brain_area_all = np.array(brain_area)

import itertools
brain_area_all = list(itertools.chain(*brain_area))
brain_area_all = np.array(brain_area_all)
print(type(brain_area_all))

kt=0
cc, sort_idx, = [np.empty(shape=(39,2),dtype='object') for _ in range(2)]
cc_brain = np.empty(shape=(39,),dtype='object')

clist=['b','r']
for isession in range(39):
    dat = alldat[isession]
    nneuron = len(dat['spks'])
    
    f = plt.figure(figsize=(15,5))
    for idum in range(2):#range(len(dummy_type)):
        cc[isession,idum] = tt[kt:kt+nneuron,which_idx[idum],idum]
        cc_brain[isession] = brain_area_all[kt:kt+nneuron]
        
        sort_idx[isession,idum] = np.argsort(np.abs(cc[isession,idum]))
        
        plt.subplot(1,2,1)
        plt.plot(cc[isession,idum],'.',color=clist[idum]); plt.grid(); plt.title('Coupling'); plt.xlabel('Neurons')
        
        plt.subplot(1,2,2)
        plt.plot(cc[isession,idum][sort_idx[isession,idum]],'.',color=clist[idum]); plt.xlabel('Sorted neurons')
        plt.title('sorted by abs value'); plt.grid()
    
    plt.plot(np.inf,np.inf,'.',color=clist[0],label=dummy_type[0])
    plt.plot(np.inf,np.inf,'.',color=clist[1],label=dummy_type[1])
    plt.legend()
    
    kt+=nneuron
    plt.suptitle(f'Session: {isession}')
    # plt.savefig(f'Figures/Time-slice coupling_session{isession:d}.png')
    





f=plt.figure(figsize=(20,5))
for idum in range(len(dummy_type)):
    plt.plot(rr[:,idum],'-o',label=dummy_type[idum])
plt.xlabel('Neuron'); plt.ylabel('psuedo-R^2 of fit')
plt.show()
    
f=plt.figure(figsize=(7,5))
plt.bar(range(len(dummy_type)),np.mean(rr,axis=0)); plt.ylabel('Pseudo-r2 of fits with coupling to:')
plt.xticks(range(len(dummy_type)),dummy_type); plt.show()

import pickle

f = open('coupling_matrix.pckl', 'wb')
pickle.dump([cc,cc_brain],f) #pickle.dump(rr,f); pickle.dump(cc,f); pickle.dump(cc_brain,f)
f.close()

f = open('coupling_matrix.pckl', 'rb')
obj1,obj2 = pickle.load(f)
f.close()

f = open('coupling_matrix.pckl', 'rb')
couplings, coupling_brain = pickle.load(f)

which_session = 11
f = plt.figure(figsize=(15,5))
# To plot couplings to session 11
plt.subplot(1,2,1); plt.plot(couplings[which_session,0],'.',label='Right contrast'); plt.grid()
plt.plot(couplings[which_session,1],'.',label='Go-NoGo'); plt.xlabel('Neurons'); plt.ylabel('Coupling weights')

sort_couplings = np.argsort(np.abs(couplings[which_session,0]))
plt.subplot(1,2,2); plt.plot(couplings[which_session,0][sort_couplings],'.',label='Right contrast'); plt.grid();plt.xlabel('Neurons')

sort_couplings = np.argsort(np.abs(couplings[which_session,1]))
plt.plot(couplings[which_session,1][sort_couplings],'.',label='Go-NoGo'); plt.xlabel('Neurons'); plt.ylabel('Coupling weights')

plt.legend()
plt.suptitle(f'Session: {which_session:d}'); plt.show()



regions = ["vis ctx", "thal", "hipp", "other ctx", "midbrain", "basal ganglia", "cortical subplate", "other"]

sorted_brain_groups = [ ['VISrl', 'VISl', 'VISpm', 'VISp', 'VISa', 'VISam'],
['POL', 'MG', 'PT', 'LP', 'LGd', 'SPF', 'LD', 'PO', 'MD', 'VAL', 'VPL', 'VPM', 'TH','RT', 'CL', 'LH'],
['CA', 'CA2', 'CA3', 'POST', 'CA1', 'SUB', 'DG'],
['DP', 'COA', 'ILA', 'ORBm', 'RSP', 'OLF', 'PL', 'ACA', 'MOs', 'MOp', 'SSs', 'SSp','ORB', 'PIR', 'AUD', ' TT'],
['SCsg', 'NB', 'APN', 'PAG', 'SCs', 'SCig', 'MRN', 'IC', 'SCm', 'MB', 'RN', 'ZI'],
['LSc', 'LS', 'LSr', 'MS', 'ACB', 'GPe', 'OT', 'CP', 'SI', 'SNr'],
['BMA', 'EP', 'EPd', 'MEA', 'BLA'] ]


all_brain_groups = []; all_area = []
for sublist in range(len(sorted_brain_groups)):
    for item in range(len(sorted_brain_groups[sublist])):
        all_brain_groups.append(sorted_brain_groups[sublist][item])
        all_area.append(sublist)

        
# col_list = ['blue','green','red','cyan','magenta','yellow','black']
col_list=['b', 'g', 'r', 'c', 'm', 'y', 'k']
which_color=1
f = plt.figure(figsize=(15,7))

for idum in range(2):
    for ib in range(len(all_brain_groups)):
        which_color=all_area[ib]
        idx_by_brain=brain_area_all == all_brain_groups[ib]
        plt.subplot(2,1,idum+1)
        try:
            if np.sum(idx_by_brain)==0:
                yy=0
            else:
                yy=np.mean(np.abs(tt[idx_by_brain,which_idx[idum],idum]))
        except:
            pdb.set_trace()
        plt.title(f'Coupling to {dummy_type[idum]}')
        plt.bar(ib, yy,color=col_list[which_color])
        plt.xticks(range(len(all_brain_groups)),all_brain_groups,rotation='vertical',fontsize=12)

for icol in range(len(col_list)):
    plt.plot(np.inf,np.inf,color=col_list[icol],linewidth=2,label=regions[icol])
    plt.legend(loc='upper left')
plt.suptitle('GLM coupling weights',y=1.1,fontsize=20)
# plt.savefig('Figures/time-slice fine_brain_regions.png')
plt.show()

'''
(D) Plot mean weights by bigger brain groups (cell after next)
'''
nareas = len(regions)-1 
NN = len(brain_area_all) # number of neurons
barea = nareas * np.ones(NN, ) # last one is "other"
for j in range(nareas):
  barea[np.isin(brain_area_all, sorted_brain_groups[j])] = j # assign a number to each region

yy=np.zeros(shape=(nareas,len(dummy_type)))
f=plt.figure(figsize=(15,5))
for idum in range(2):#range(len(dummy_type)):
    plt.subplot(1,2,idum+1)
    for j in range(nareas): 
        try:
            yy[j,idum] = np.mean(np.abs(tt[barea==j,which_idx[idum],idum]))
            plt.bar(j,yy[j,idum], color=col_list[j],label=regions[j])

        except:
            pdb.set_trace()

        plt.title(f'Coupling to {dummy_type[idum]}')
        plt.ylabel('Coupling'); plt.xlabel('Brain areas')

    plt.xticks(range(len(regions)-1),regions[:-1],rotation='vertical',fontsize=12)
# plt.savefig('Figures/time-slice_coarse_regions.png')

hat = ['','/']
yy_norm = np.zeros_like(yy)
f=plt.figure(figsize=(7,5))
for idum in range(2):
    yy_norm[:,idum] = yy[:,idum]/np.sum(yy[:,idum])
    plt.bar(np.inf,np.inf,hatch=hat[idum],edgecolor='black',color='white', label=dummy_type[idum])
    for j in range(nareas):
        plt.bar(j-0.1+0.3*idum,yy_norm[j,idum],width=0.2,color=col_list[j],hatch=hat[idum])
    plt.xticks(range(len(regions)-1),regions[:-1],rotation='vertical',fontsize=12)
plt.legend(loc='lower right'); plt.ylabel('Normalised coupling')
# plt.savefig('Figures/time-slice_coarse_regions_compare.png')
plt.show();    


# This cell seems incomplete
# '''
# Make [nneuron x 4] matrix: 
# Columns: session #, brain region, coupling to right contrast, coupling to go/nogo, 
# '''
# 
# cmat = np.empty(shape=(len(brain_area_all),3))
# for i in range(39): #range(len(brain_area_all)):
#     dat = alldat['spks']
#     nneuron = 
#     cmat[i,0] 


'''
Plot r^2 of fits for each brain region
'''
f=plt.figure(figsize=(7,5))
for idum in range(len(dummy_type)):
    plt.subplot(1,2,idum+1)
    for j in range(nareas): 
        try:
            yy = np.mean((rr[barea==j,idum]))
            plt.bar(j,yy, color=col_list[j],label=regions[j])

        except:
            pdb.set_trace()

        plt.title(f'Pseudo-$r^2$ of fits with {dummy_type[idum]}')
        plt.xlabel('Brain areas')
        plt.xticks(range(len(regions)-1),regions[:-1],rotation='vertical',fontsize=12)



kt=0
cc=np.empty(shape=(39,2),dtype='object')

for isession in range(39):
    dat = alldat[isession]
    nneuron = len(dat['spks'])
    cc[isession,0] = tt[kt:kt+nneuron,2,0]
    cc[isession,1] = tt[kt:kt+nneuron,1,1]
    kt+=nneuron
    
    sort_idx1 = np.argsort(np.abs(cc[isession,0]))
    sort_idx2 = np.argsort(np.abs(cc[isession,1]))
    
    f = plt.figure(figsize=(15,5))
    plt.subplot(1,2,1); plt.plot(cc[isession,0][sort_idx1],'.'); plt.title('right contrast coupling')
    plt.subplot(1,2,2); plt.plot(cc[isession,1][sort_idx2],'.'); plt.title('go-nogo coupling')
    plt.suptitle(f'Session: {isession}')
    

'''
1. Divide data into train-test. Preserve chronology!
2. Encode 
    (a) contrast difference, 
    (b) right contrast in Poisson GLM on train data
3. Test accuracy on test data.
    (a) MSE
    (b) R^2

'''
# Initialise variables for Poisson GLM fitting
L1_penalty = 1

# Define types of stimuli
stim_type =['Contrast difference','Left contrast','Right contrast']

# Get index of train and test data
idx_train = range(0,int(len(cdiff)*0.75)); idx_test = range(int(len(cdiff)*0.75), len(cdiff))

# Divide stimuli into train and test 
stim_train = np.empty(shape=(len(idx_train),len(stim_type)))
stim_test = np.empty(shape=(len(idx_test),len(stim_type)))

stim_train[:,0] = cdiff[idx_train]; stim_test[:,0] = cdiff[idx_test] # Stimulus 1: Contrast difference
stim_train[:,1] = cL[idx_train]; stim_test[:,1] = cL[idx_test]  # Stimulus 2: Right contrast
stim_train[:,2] = cR[idx_train]; stim_test[:,2] = cR[idx_test]  # Stimulus 2: Right contrast
dummy_train = abs(dat['response'][idx_train])>0

# Divide spikes into train and test
spikes_train = all_spikes[idx_train,:]; spikes_train = spikes_train[:, vis_neurons_thresh]
spikes_test = all_spikes[idx_test,:]; spikes_test = spikes_test[:, vis_neurons_thresh]
dummy_test = abs(dat['response'][idx_test])>0

# List d parameters
d_list = range(2,30,2)

# Initialize variable for each L1 penalty
ytrain = np.empty(shape=(len(idx_train),len(stim_type),len(d_list),len(vis_neurons_thresh)))
ytest = np.empty(shape=(len(idx_test),len(stim_type),len(d_list),len(vis_neurons_thresh)))
train_accuracy, test_accuracy = [np.empty(shape=(len(stim_type),len(d_list),len(vis_neurons_thresh))) for _ in range(2)]

# Initialize theta_d to store temporal filters for d=10 values
theta_d = np.empty(shape=(11,len(stim_type),len(vis_neurons_thresh)))

# Fit to training data and evaluate on test data
for chosen_neuron in range(len(vis_neurons_thresh)):
    print(vis_neurons_thresh[chosen_neuron])
    for d1 in range(len(d_list)):
        for istim in range(len(stim_type)):
            ytrain[:,istim,d1,chosen_neuron], theta = predict_spike_counts_lnp(
                stim_train[:,istim], dummy_train, spikes_train[:,chosen_neuron],d_list[d1],L1_penalty)
            ytest[:,istim,d1,chosen_neuron],_ = predict_spike_counts_lnp(
                stim_test[:,istim], dummy_test, spikes_test[:,chosen_neuron],d_list[d1],L1_penalty,theta)

            # Evaluate 'accuracy' on train and test data
            train_accuracy[istim,d1,chosen_neuron] = np.mean(
                np.divide(((ytrain[:,istim,d1,chosen_neuron] - spikes_train[:,chosen_neuron])**2), ytrain[:,istim,d1,chosen_neuron])
            )
            test_accuracy[istim,d1,chosen_neuron] = np.mean(
                np.divide(((ytest[:,istim,d1,chosen_neuron] - spikes_test[:,chosen_neuron])**2), ytest[:,istim,d1,chosen_neuron])
            )
            
           #save theta for d=9
            if d_list[d1]==9:
                theta_d[:,istim,chosen_neuron] = theta;  



# Plot train data for d = 20 and a good neuron (100)
whichd=1; which_neuron = 1
f=plt.figure(figsize=(20,10))
for istim in range(len(stim_type)):
        plt.subplot(len(stim_type),1,istim+1); plt.plot(ytrain[:,istim,whichd,which_neuron],'o-',label='Predicted'); plt.title(stim_type[istim])
        markerline, stemlines, baseline = plt.stem(spikes_train[:,which_neuron],linefmt='grey',basefmt='grey',label='spikes', use_line_collection=True)
        markerline.set_markerfacecolor('grey')
        plt.setp(markerline,color="0.5")

plt.suptitle(f'Neuron {chosen_neuron:d}, Train data', y=1.1, fontsize=20)    
plt.legend()
# plt.savefig('Figures/Train data.png')
plt.show()

# Plot test data 
f=plt.figure(figsize=(20,10))
for istim in range(len(stim_type)):
    plt.subplot(len(stim_type),1,istim+1)
    markerline, stemlines, baseline = plt.stem(spikes_test[:,which_neuron],linefmt='grey',basefmt='grey',use_line_collection=True,label='Spikes')
    markerline.set_markerfacecolor('grey'); plt.setp(markerline,color="0.5")
    plt.plot(ytest[:,istim,whichd,which_neuron],'o-',label='Predicted spikes')
    plt.title(stim_type[istim]); plt.xlabel('Trial #'); plt.ylabel('Spikes')
    
plt.suptitle('Test data', y=1.1, fontsize=20)
plt.legend(loc='upper center',bbox_to_anchor=(0.5, -0.3),fancybox=False, shadow=False,prop={'size':20})

# plt.legend(bbox_to_anchor=(0, 1), loc='upper left',prop={'size': 20})

# plt.savefig('Figures/Test data.png')
plt.show()


# Plot train-test accuracy for each neuron
f=plt.figure(figsize=(15,5))
for istim in range(len(stim_type)):
    for which_neuron in range(len(vis_neurons_thresh)):
        plt.subplot(1,len(stim_type),istim+1); plt.plot(d_list, train_accuracy[istim,:,which_neuron],'o-b',label='Train accuracy')
        plt.plot(d_list,test_accuracy[istim,:,which_neuron],'o-r',label='Test accuracy')
        plt.title(stim_type[istim]); plt.xlabel('#history points included for encoding'); plt.ylabel('Prediction error')
 
plt.show()

# Plot average train-test accuracy
f=plt.figure(figsize=(15,5))
for istim in range(len(stim_type)):
    
    yy_train = np.mean(train_accuracy[istim,:,:],axis=1)
    yy_test = np.mean(test_accuracy[istim,:,:],axis=1)
    err_train = np.std(train_accuracy[istim,:,:],axis=1)/np.sqrt(len(train_accuracy[istim,0,]))
    err_test = np.std(test_accuracy[istim,:,:],axis=1)/np.sqrt(len(test_accuracy[istim,0,]))
    
    plt.subplot(1,len(stim_type),istim+1); plt.plot(d_list,yy_train,'o-b',label='Train error')
    plt.plot(d_list,yy_test,'o-r',label='Test error')
    plt.fill_between(d_list,yy_train-err_train,yy_train+err_train,facecolor='b',alpha=0.2)
    plt.fill_between(d_list,yy_test-err_test,yy_test+err_test,facecolor='r',alpha=0.2)
    
    plt.ylim([2,9])
    plt.title(stim_type[istim]); plt.xlabel('#history points included for encoding'); plt.ylabel('Prediction error')

plt.suptitle('Average prediction errors across VIS-neurons',y=1.1)
plt.legend()
# plt.savefig('Figures/Test-train accuracy.png')
plt.show()


# plot temporal filters - TR: What does this even mean?

f=plt.figure(figsize=(15,5))
for istim in range(len(stim_type)):
    plt.subplot(1,len(stim_type),istim+1)
    for chosen_neuron in range(len(vis_neurons_thresh)):
        plt.plot(theta_d[:,istim,chosen_neuron],'-ob')