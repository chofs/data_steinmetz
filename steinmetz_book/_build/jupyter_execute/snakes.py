# Snake plots
PSTHs sorted by latency of peak. This is somewhat silly, because we are fitting to noise for the most part
(Hello hippocampal researchers!). However, we can look at abrupt changes in slope to find clusters of responses.


```{toctree}
:hidden:
:titlesonly:


snakes00
snakes01
snakes02
snakes03
snakes04
snakes05
snakes06
snakes07
snakes08
snakes09
snakes10
snakes11
snakes12
snakes13
snakes14
snakes15
snakes16
snakes17
snakes18
snakes19
snakes20
snakes21
snakes22
snakes23
snakes24
snakes25
snakes26
snakes27
snakes28
snakes29
snakes30
snakes31
snakes32
snakes33
snakes34
```
